resource "vsphere_virtual_machine" "vm" {
  name             = var.vm_name
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id

  num_cpus  = var.vm_cpu_count
  num_cores_per_socket = var.vm_cores_per_socket
  memory    = var.vm_memory_size
  cpu_hot_add_enabled = var.cpu_hot_add
  memory_hot_add_enabled = var.memory_hot_add
  enable_disk_uuid = var.disk_enable_disk_uuid
  guest_id  = data.vsphere_virtual_machine.template.guest_id
  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }
  
  dynamic "disk" {
    for_each = [for s in data.vsphere_virtual_machine.template.disks: {
      label =  index(data.vsphere_virtual_machine.template.disks, s)
      unit_number =  index(data.vsphere_virtual_machine.template.disks, s)
      size = s.size
      eagerly_scrub = s.eagerly_scrub
      thin_provisioned = contains(keys(s),"thin_provisioned") ? s.thin_provisioned : "true"
    }]
    content {
      label = disk.value.label
      unit_number = disk.value.unit_number
      size = disk.value.size
      datastore_id = data.vsphere_datastore.datastore.id
      eagerly_scrub = disk.value.eagerly_scrub
      thin_provisioned = disk.value.thin_provisioned
      }
  }

  dynamic "disk" {
    for_each = [for d in var.additional_disks: {
      size   = d.size
      number = d.number
      disk_mode =  contains(keys(d),"disk_mode") ? d.disk_mode : "persistent"
      disk_sharing = contains(keys(d),"disk_sharing") ? d.disk_sharing : "sharingNone"
    }]
    content {
      label            = format("disk%s", disk.value.number)
      size             = disk.value.size
      unit_number      = disk.value.number
      disk_mode        = disk.value.disk_mode
      disk_sharing     = disk.value.disk_sharing
    }
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = var.vm_name
        domain    = var.dns_domain
      }

      network_interface {
        ipv4_address = var.vm_ip
        ipv4_netmask = 24
      }

      ipv4_gateway = var.default_gw
      dns_server_list = var.dns_servers_list
      dns_suffix_list = var.resolvers_dns_domain
    }
  }
}
