output "vm_ip_address" {
  value = vsphere_virtual_machine.vm.default_ip_address
}
output "vm_id" {
  value = vsphere_virtual_machine.vm.id
}