variable "vsphere_datacenter_name" {
  description = "Vsphere DC name"
  default     = "SBI"
}

variable "vsphere_datastore_name" {
  description = "Vsphere Data Storage name"
  #default     = "Platform_NonProd_OS_01"
}

variable "vsphere_cluster_name" {
  description = "Vsphere Cluster name"
  #default     = "Platform_NonProd"
}

variable "vsphere_network_name" {
  description = "Vsphere network name"
#  default     = "NONPROD_APP_676"
}

variable "default_gw" {
  description = "VLAN Default GW"
#  default     = "10.19.168.254"
}

variable "vsphere_vm_template_name" {
  description = "Vsphere VM template name"
#  default     = "ol7_dynamic_disk"
}

variable "vm_cpu_count" {
  description = "VM number of CPU cores"
  default     = "2"
}

variable "vm_cores_per_socket" {
  description = "VM number of core per vCPU"
  default     = "1"
}

variable "vm_memory_size" {
  description = "VM memory size in MB"
  default     = "4096"
}

variable "vm_name" {
  description = "Server Name in vCenter"  
}

variable "vm_ip" {
  description = "Server IP"  
}

# additional disks
variable "additional_disks" {
  type    = list
  default = []
}


variable "dns_domain" {
  description = "Defautl DNS domain zone"
  default     = "hcnet.vn"
}

variable "resolvers_dns_domain" {
  description = "Defautl DNS domain zone"
  default     = ["hcnet.vn"]
}

variable "dns_servers_list" {
  type    = list(string)
  description = "DNS servers"
  default     = ["10.19.173.5", "10.19.173.6"]
}

# cpu and memory hot add
variable "cpu_hot_add" {
  description = "CPU hot add to VMs enabled"
  default = true
}

variable "memory_hot_add" {
  description = "Memory hot add to VMs enabled"
  default = true
}

variable "disk_enable_disk_uuid" {
  description = "Enable disk uuid"
  default = false
} 